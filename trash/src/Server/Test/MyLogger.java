package Server.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.*;

public class MyLogger {
    private static Logger logger;

    private static boolean isSet;
    public static Logger getMyLogger(String name) {
        logger = Logger.getLogger(name);
        if (isSet == false) {
            try {
                LogManager.getLogManager().readConfiguration(new FileInputStream("C:\\Users\\trinh" +
                        "\\OneDrive\\Documents\\20202\\NetProg\\project\\Bang2\\src\\resources\\mylogging.properties"));
            } catch (SecurityException | IOException e1) {
                e1.printStackTrace();
            }
            logger.setLevel(Level.FINE);
            logger.addHandler(new ConsoleHandler());
            //adding custom handler
            logger.addHandler(new MyHandler());
            try {
                //FileHandler file name with max size and number of log files limit
                Handler fileHandler = new FileHandler("C:\\Users\\trinh\\OneDrive" +
                        "\\Documents\\20202\\NetProg\\project\\Bang2\\src\\resources\\logger.txt");
                fileHandler.setFormatter(new MyFormatter());
                //setting custom filter for FileHandler
                fileHandler.setFilter(new MyFilter());
                logger.addHandler(fileHandler);
                logger.log(Level.CONFIG, "Config data");
            } catch (SecurityException | IOException e) {
                e.printStackTrace();
            }
            isSet = true;
        }
        return logger;
    }
}
