package Server.database.connection;

import Server.database.DatabaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL implements DatabaseConnection {
    private static Connection connection;
    private static final String url = "jdbc:mysql://127.0.0.1:3306/netprog";
    private static final String username = "root";
    private static final String password = "root";

    public static Connection getConnection() throws SQLException {
        if(connection == null) {
            connection = DriverManager.getConnection(url, username, password);
        }
        return connection;
    }
}
