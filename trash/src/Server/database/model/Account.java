package Server.database.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Account {
    private static final String SQL_INSERT = "Insert into account values (?, ? , 0)";
    private static final String SQL_UPDATE = "Update account set online = ? where username = ?";
    private static final String SQL_SELECT = "Select * from account where username = ?";

    private String username;
    private String password;
    private boolean online;

    public boolean insertAccount(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, password);

        int row = preparedStatement.executeUpdate();
        return row == 0 ? false : true;
    }

    public boolean updateAccount(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);
        preparedStatement.setBoolean(1, online);
        preparedStatement.setString(2, username);

        int row = preparedStatement.executeUpdate();
        return row == 0 ? false : true;
    }

    public boolean checkAccountExist(Connection connection, String username) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setString(1, username);

        int row = preparedStatement.executeUpdate();
        return row == 0 ? false : true;
    }

    public Account(String username, String password, boolean online) {
        this.username = username;
        this.password = password;
        this.online = online;
    }

    public Account() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
