package Test;

public class Child extends Father {
    int age;

    public Child() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Child(int age) {
        this.age = age;
    }
}
