package Server.handler.transmission;

import Server.controller.IController;
import Server.entity.app.Client;
import Server.pattern.factory.ControllerFactory;
import Server.utils.json.JsonHandler;

import java.util.ArrayList;

public class ReceiveHandler { // handling json request and direct it to controller
    IController iController;

    public ReceiveHandler(String receivedJson, String role) {
        String command = JsonHandler.getStringAttribute(receivedJson, "command");
        iController = ControllerFactory.getController(command, role);
    }

    public ArrayList<String> processRequest(String receive, Client client) {
        return iController.prepareJson(receive, client);
    }
}
