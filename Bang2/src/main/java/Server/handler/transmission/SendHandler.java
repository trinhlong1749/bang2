package Server.handler.transmission;

import Server.dispatcher.IDispatcher;
import Server.entity.app.Client;
import Server.handler.connection.ClientHandler;
import Server.pattern.factory.DispatcherFactory;
import Server.utils.json.JsonHandler;

import java.util.ArrayList;

public class SendHandler {
    IDispatcher iDispatcher;

    public SendHandler(String returnJson) {
        String type = JsonHandler.getStringAttribute(returnJson, "type");
        iDispatcher = DispatcherFactory.getDispatcher(type);
    }

    public ArrayList<ClientHandler> processReceivers(Client client, String returnJson) {
        return iDispatcher.prepareReceivers(client, returnJson);
    }
}
