//package Server.handler.transmission;
//
//import Server.entity.app.Client;
//import Server.notifier.LobbyNotifier;
//
//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.net.Socket;
//
//public class BroadcastHandler extends Thread {
//
//    DataOutputStream dos;
//    DataInputStream dis;
//    Socket socket;
//    Client client;
//
//    LobbyNotifier lobbyNotifier = new LobbyNotifier();
//
//    public BroadcastHandler(DataOutputStream dos, DataInputStream dis, Socket socket, Client client) {
//        this.dos = dos;
//        this.dis = dis;
//        this.socket = socket;
//        this.client = client;
//    }
//
//    @Override
//    public void run() {
//        while(true) {
//            String lobbyBroadcast;
//            if(client.isBroadcastLobby()) {
//                lobbyBroadcast = lobbyNotifier.prepareNotify(this.client);
//                try {
//                    dos.writeUTF(lobbyBroadcast);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }
//}
