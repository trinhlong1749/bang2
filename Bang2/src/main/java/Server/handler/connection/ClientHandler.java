package Server.handler.connection;

import Server.entity.app.Client;
import Server.entity.app.Guest;
import Server.entity.app.Lobby;
import Server.entity.app.User;
import Server.entity.app.game.Player;
//import Server.handler.transmission.BroadcastHandler;
import Server.handler.transmission.ReceiveHandler;
import Server.handler.transmission.SendHandler;
import Server.notifier.LobbyNotifier;
import Server.server.Server;
import Server.utils.json.JsonHandler;
import Server.utils.logger.LoggingExample;
import Server.utils.logger.MyLogger;
import com.google.gson.JsonObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientHandler implements Runnable {
    static Logger logger = MyLogger.getMyLogger(ClientHandler.class.getName());

    private final DataInputStream dataInputStream;
    private final DataOutputStream dataOutputStream;
    private final Socket socket;

    ReceiveHandler receiveHandler;
    SendHandler sendHandler;
//    LobbyNotifier lobbyNotifier = new LobbyNotifier();

    private Client client = new Client();

    public ClientHandler(Socket s, DataInputStream dis, DataOutputStream dos) {
        dataInputStream = dis;
        dataOutputStream = dos;
        socket = s;

        client.setGuest(new Guest());
    }

    @Override
    public void run() {
        String received;
        String toreturn;

        while(true) {
            try {
                dataOutputStream.writeUTF("Your json command: "); // TODO: terminate thi loi o day nay
                received = dataInputStream.readUTF();
                logger.log(Level.INFO, "Current thread: " + Thread.currentThread().getId());
                if (received.equals("Exit") ) {
                    exit(client);
                    System.out.println("Client " + this.socket + " sends exit...");
                    System.out.println("Closing this connection.");
                    this.socket.close();
                    System.out.println("Connection closed");
                    break;
                }

                logger.log(Level.INFO, "Receive: " + received);
                logger.log(Level.INFO, "Finding suitable controller for client request");

                receiveHandler = new ReceiveHandler(received, client.getRole()); // check role vs command
                ArrayList<String> returnJsons = receiveHandler.processRequest(received, client);

                ArrayList<ClientHandler> outputTargets;
                for(String returnJson : returnJsons) {
                    // outputTargets = IDispatcher.prepareTarget();
                    sendHandler = new SendHandler(returnJson);
                    outputTargets = sendHandler.processReceivers(client, returnJson);
                    for(ClientHandler ch : outputTargets) {
//                    for(ClientHandler ch : Server.clients) {
                        ch.dataOutputStream.writeUTF("Your return json: " + returnJson);
                        logger.log(Level.INFO, "Return: " + returnJson);
                    }
                }

                if (client != null) logger.log(Level.INFO, "Client role: " + client.getRole());

                //cuoi cung se dung sendhandler de gui update state (room, lobby, game, etc...)
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    this.dataInputStream.close();
                    this.dataOutputStream.close();
                    this.socket.close();
                    exit(client);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
        try {
            // closing resources
            this.dataInputStream.close();
            this.dataOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setClient(Client client) {
        this.client = client;
    }

    public void sendMsg(String msg) throws IOException {
        dataOutputStream.writeUTF(msg);
    }

    private void exit(Client client) {
        if (!("guest".equals(client.getRole()))) {
            String logout = "{command:logout}";
            client.roleToUser(new Guest(), client.getUsername());
            receiveHandler = new ReceiveHandler(logout, client.getRole());
            receiveHandler.processRequest(logout, client);
        }
    }

    public Client getClient() {
        return client;
    }
}
