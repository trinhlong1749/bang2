package Server.utils.logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingExample {

    static Logger logger = MyLogger.getMyLogger(LoggingExample.class.getName());

    public static void main(String[] args) throws IOException {
        for(int i=0; i<1000; i++){
            //logging messages
            logger.log(Level.INFO, "Msgs"+i);
        }
        logger.log(Level.CONFIG, "Config data");
    }

}
