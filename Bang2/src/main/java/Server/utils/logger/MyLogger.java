package Server.utils.logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.logging.*;

public class MyLogger {
    private static Logger logger;

    static ClassLoader classLoader = MyLogger.class.getClassLoader();

    private static boolean isSet;
    public static Logger getMyLogger(String name) {
        logger = Logger.getLogger(name);

        if (isSet == false) {
            try {
//                LogManager.getLogManager().readConfiguration(new FileInputStream("C:\\Users\\trinh" +
//                        "\\OneDrive\\Documents\\20202\\NetProg\\project\\Bang2\\src\\resources\\mylogging.properties"));
                LogManager.getLogManager().readConfiguration(new FileInputStream(Paths.get(classLoader.getResource("mylogging.properties").toURI()).toString()));
            } catch (SecurityException | IOException | URISyntaxException e1) {
                e1.printStackTrace();
            }
            logger.setLevel(Level.FINE);
            logger.addHandler(new ConsoleHandler());
            //adding custom handler
            logger.addHandler(new MyHandler());
            try {
                //FileHandler file name with max size and number of log files limit'
                System.out.println(Paths.get(classLoader.getResource("logger.log").toURI()).toString());
                Handler fileHandler = new FileHandler(Paths.get(classLoader.getResource("logger.log").toURI()).toString());
                fileHandler.setFormatter(new MyFormatter());
                //setting custom filter for FileHandler
                fileHandler.setFilter(new MyFilter());
                logger.addHandler(fileHandler);
                logger.log(Level.CONFIG, "Config data");
            } catch (SecurityException | IOException | URISyntaxException e) {
                e.printStackTrace();
            }
            isSet = true;
        }
        return logger;
    }
}
