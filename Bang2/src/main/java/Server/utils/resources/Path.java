package Server.utils.resources;

import java.net.URISyntaxException;
import java.nio.file.Paths;

public class Path {
    static private ClassLoader classLoader = Path.class.getClassLoader();

    public static String getPathFromResources(String name) throws URISyntaxException {
        return Paths.get(classLoader.getResource(name).toURI()).toString();
    }
}
