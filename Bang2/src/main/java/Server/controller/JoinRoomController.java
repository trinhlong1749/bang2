package Server.controller;

import Server.entity.app.Client;
import Server.entity.app.Lobby;
import Server.entity.app.Room;
import Server.entity.transmission.broadcast.RoomNotify;
import Server.entity.transmission.joinroom.JoinRoomReceive;
import Server.entity.transmission.joinroom.JoinRoomSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JoinRoomController implements IController{
    static Logger logger = MyLogger.getMyLogger(CreateRoomController.class.getName());

    private JoinRoomReceive joinRoomReceive;
    private JoinRoomSend joinRoomSend;
    private RoomNotify roomNotify;

    public JoinRoomController() {
    }

    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Preparing return json");
        String succeedMsg = "succeed";
        String failedMsg = "failed";

        ArrayList<String> returnJsons = new ArrayList<String>();

        roomNotify = new RoomNotify();
        joinRoomReceive = JsonHandler.getGson().fromJson(receive, JoinRoomReceive.class);
        boolean result = processRequest(joinRoomReceive, client);
        String roomid = joinRoomReceive.getRoomid();
        Room room = Lobby.getLobby().getRoom(roomid);

        try {
            joinRoomSend = new JoinRoomSend();
            joinRoomSend.setResult(result);
            joinRoomSend.setCommand("joinroom");
            joinRoomSend.setMsg(result ? succeedMsg : failedMsg);
            joinRoomSend.setRoomMembers(result ? room.getMembers() : null);
            returnJsons.add(JsonHandler.getGson().toJson(joinRoomSend));
            if(result) {
                roomNotify.setMembers(room.getMembers());
                roomNotify.setCommand("roomnotify");
                roomNotify.setRoom(room);
                roomNotify.setMsg("Notify room status");
                returnJsons.add(JsonHandler.getGson().toJson(roomNotify));
            }
        } catch (Exception e) {
            joinRoomSend = new JoinRoomSend();
            joinRoomSend.setResult(false);
            joinRoomSend.setCommand("joinroom");
            joinRoomSend.setMsg(failedMsg);
            joinRoomSend.setRoomMembers(null);
            returnJsons.add(JsonHandler.getGson().toJson(joinRoomSend));
        }

        return returnJsons;
    }

    private boolean processRequest(JoinRoomReceive joinRoomReceive, Client client) {
        try {
            String roomid = joinRoomReceive.getRoomid();
            Room room = Lobby.getLobby().getRoom(roomid);
            if(room.isMax() || roomid.equalsIgnoreCase(client.getUsername())) return false;
            room.addMember(client.getUser());
            client.setRoom(room);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
