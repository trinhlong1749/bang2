package Server.controller;

import Server.entity.app.Client;
import Server.entity.app.Room;
import Server.entity.transmission.broadcast.RoomNotify;
import Server.entity.transmission.leaveroom.LeaveRoomReceive;
import Server.entity.transmission.leaveroom.LeaveRoomSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LeaveRoomController implements IController{
    static Logger logger = MyLogger.getMyLogger(LeaveRoomController.class.getName());

    private LeaveRoomReceive leaveRoomReceive;
    private LeaveRoomSend leaveRoomSend;
    private RoomNotify roomNotify;

    public LeaveRoomController() {

    }
    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Prepare return json");

        ArrayList<String> returnJsons = new ArrayList<>();

        roomNotify = new RoomNotify();
        leaveRoomReceive = JsonHandler.getGson().fromJson(receive, LeaveRoomReceive.class);
        leaveRoomSend = new LeaveRoomSend();

        Room room = client.getRoom();
        String succeedMsg = "Succeed";
        String failedMsg = "Failed";

        try {
            boolean result = processRequest(leaveRoomReceive, client);
            leaveRoomSend.setCommand("leaveroom");
            leaveRoomSend.setCommand(result ? succeedMsg : failedMsg);
            leaveRoomSend.setResult(result);
            returnJsons.add(JsonHandler.getGson().toJson(leaveRoomSend));
            roomNotify.setRoom(room);
            roomNotify.setMsg("Notify room Status");
            roomNotify.setCommand("roomnotify");
            roomNotify.setMembers(room.getMembers());
            returnJsons.add(JsonHandler.getGson().toJson(roomNotify));
        } catch (Exception e) {
            leaveRoomSend.setCommand("leaveroom");
            leaveRoomSend.setCommand(failedMsg);
            leaveRoomSend.setResult(false);
        }

        return returnJsons;
    }

    private boolean processRequest(LeaveRoomReceive leaveRoomReceive, Client client) throws IOException {
        return client.leaveRoom();
    }
}
