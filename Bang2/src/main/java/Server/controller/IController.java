package Server.controller;

import Server.entity.app.Client;
import Server.entity.transmission.Receive;

import java.util.ArrayList;

public interface IController {
    public ArrayList<String> prepareJson(String receive, Client client);
}
