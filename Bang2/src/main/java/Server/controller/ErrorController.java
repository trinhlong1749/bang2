package Server.controller;

import Server.entity.app.Client;
import Server.utils.logger.MyLogger;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorController implements IController{
    static Logger logger = MyLogger.getMyLogger(LoginController.class.getName());

    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Invalid request from client");
        ArrayList<String> returnJsons = new ArrayList<>();
        returnJsons.add(new String("Invalid request"));
        return returnJsons;
    }
}
