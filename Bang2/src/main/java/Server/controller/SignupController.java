package Server.controller;

import Server.entity.app.Client;
import Server.entity.database.connection.MySQL;
import Server.entity.database.model.Account;
import Server.entity.transmission.signup.SignupReceive;
import Server.entity.transmission.signup.SignupSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignupController implements IController {
    static Logger logger = MyLogger.getMyLogger(SignupController.class.getName());

    SignupReceive signupReceive;
    SignupSend signupSend;

    public SignupController() {
        System.out.println("Signup Controller created");
    }

    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Prepare return Json");

        signupSend = new SignupSend();
        SignupReceive signupReceive = JsonHandler.getGson().fromJson(receive, SignupReceive.class);
        String succeedMsg = "succeed";
        String failedMsg = "failed";
        try {
            boolean result = processRequest(signupReceive);
            signupSend.setResult(result);
            signupSend.setMsg(result == true ? succeedMsg : failedMsg);
            signupSend.setCommand("signup");
        } catch (SQLException e) {
            signupSend.setResult(false);
            signupSend.setMsg(failedMsg);
            signupSend.setCommand("signup");
        }

        ArrayList<String> returnJsons = new ArrayList<>();
        returnJsons.add(JsonHandler.getGson().toJson(signupSend));
        return returnJsons;
    }

    private boolean processRequest(SignupReceive signupReceive) throws SQLException {
        logger.log(Level.INFO, "Update database");

        Account account = new Account(signupReceive.getUsername(), signupReceive.getPassword());
        return account.insertAccount(MySQL.getConnection());
    }
}
