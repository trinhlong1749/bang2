package Server.controller;

import Server.entity.app.Client;
import Server.entity.app.Lobby;
import Server.entity.app.Room;
import Server.entity.transmission.broadcast.LobbyNotify;
import Server.entity.transmission.createroom.CreateRoomReceive;
import Server.entity.transmission.createroom.CreateRoomSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateRoomController implements IController {
    static Logger logger = MyLogger.getMyLogger(CreateRoomController.class.getName());

    private CreateRoomReceive createRoomReceive;
    private CreateRoomSend createRoomSend;
    private  LobbyNotify lobbyNotify;
    public CreateRoomController() {

    }
    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Prepare return json");

        ArrayList<String> returnJsons = new ArrayList<>();

        lobbyNotify = new LobbyNotify();
        createRoomSend = new CreateRoomSend();
        createRoomReceive = JsonHandler.getGson().fromJson(receive, CreateRoomReceive.class);

        String succeedMsg = "succeed";
        String failedMsg = "failed";
        try {
            boolean result = processRequest(createRoomReceive, client);
            if(!result) client.roleToUser(client.getPlayer());
            createRoomSend.setResult(result);
            createRoomSend.setCommand("createroom");
            createRoomSend.setMsg(result ? succeedMsg : failedMsg);
            createRoomSend.setRoomid(result ? client.getUsername() : null);
            returnJsons.add(JsonHandler.getGson().toJson(createRoomSend));
            if(result) {
                lobbyNotify.setCommand("lobbynotify");
                lobbyNotify.setMsg("Notify lobby");
                lobbyNotify.setRooms(Lobby.getLobby().getRooms());
                returnJsons.add(JsonHandler.getGson().toJson(lobbyNotify));
            }
        } catch (Exception e) {
            createRoomSend.setResult(false);
            createRoomSend.setRoomid(null);
            createRoomSend.setCommand("createroom");
            createRoomSend.setMsg("failed me roi tdn nhi");
            returnJsons.add(JsonHandler.getGson().toJson(createRoomSend));
            e.printStackTrace();
        }
        return returnJsons;
    }

    private boolean processRequest(CreateRoomReceive createRoomReceive, Client client) throws IOException {
//        client.roleToPlayer(client.getUser());
        Room room = new Room();
        room.setHostname(client.getUsername());
        room.addMember(client.getUser());
        Lobby lobby = Lobby.getLobby();
        lobby.addRoom(room);
        client.setRoom(room);
        return true;
    }
}
