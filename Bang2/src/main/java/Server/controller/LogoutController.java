package Server.controller;

import Server.entity.app.Client;
import Server.entity.app.Lobby;
import Server.entity.database.connection.MySQL;
import Server.entity.database.model.Account;
import Server.entity.transmission.logout.LogoutReceive;
import Server.entity.transmission.logout.LogoutSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogoutController implements IController {
    static Logger logger = MyLogger.getMyLogger(LoginController.class.getName());

    LogoutReceive logoutReceive;
    LogoutSend logoutSend;

    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Prepare return Json");

        logoutSend = new LogoutSend();
        logoutReceive = JsonHandler.getGson().fromJson(receive, LogoutReceive.class);

        String succeedMsg = "succeed logout";
        String failedMsg = "failed logout";

        try {
            logoutReceive.setUsername(client.getUsername());
            boolean result = processRequest(logoutReceive);
            if(result) client.roleToGuest();
            logoutSend.setResult(result);
            logoutSend.setMsg(result ? succeedMsg : failedMsg);
            logoutSend.setCommand("logout");
        } catch (SQLException e) {
            logoutSend.setResult(false);
            logoutSend.setMsg(failedMsg);
            logoutSend.setCommand("logout");
        }

        ArrayList<String> returnJsons = new ArrayList<>();
        returnJsons.add(JsonHandler.getGson().toJson(logoutSend));
        return returnJsons;
    }

    private boolean processRequest(LogoutReceive logoutReceive) throws SQLException {
        Account account = new Account();
        account.setUsername(logoutReceive.getUsername());
        account.setOnline(false);
        return account.updateAccount(MySQL.getConnection());
    }
}
