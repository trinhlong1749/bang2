package Server.controller;

import Server.entity.app.Client;
import Server.entity.app.Guest;
import Server.entity.app.Lobby;
import Server.entity.database.connection.MySQL;
import Server.entity.database.model.Account;
import Server.entity.transmission.login.LoginReceive;
import Server.entity.transmission.signup.SignupReceive;
import Server.entity.transmission.login.LoginSend;
import Server.utils.json.JsonHandler;
import Server.utils.logger.MyLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController implements IController{
    static Logger logger = MyLogger.getMyLogger(LoginController.class.getName());

    private LoginReceive loginReceive;
    private LoginSend loginSend;

    public LoginController() {
        System.out.println("Login Controller created");
    }

    @Override
    public ArrayList<String> prepareJson(String receive, Client client) {
        logger.log(Level.INFO, "Prepare return Json");

        loginSend = new LoginSend();

        LoginReceive loginReceive = JsonHandler.getGson().fromJson(receive, LoginReceive.class);
        String succeedMsg = "succeed";
        String failedMsg = "failed";
        try {
            boolean result = processRequest(loginReceive);
            if(result) client.roleToUser(new Guest(), loginReceive.getUsername());
            loginSend.setResult(result);
            loginSend.setMsg(result ? succeedMsg : failedMsg);
            loginSend.setLobby(Lobby.getLobby());
            loginSend.setCommand("login");
        } catch (SQLException e) {
            loginSend.setResult(false);
            loginSend.setMsg(failedMsg);
            loginSend.setCommand("login");
        }

        ArrayList<String> returnJsons = new ArrayList<>();
        returnJsons.add(JsonHandler.getGson().toJson(loginSend));
        return returnJsons;
    }

    private boolean processRequest(LoginReceive loginReceive) throws SQLException {
        Account account = new Account(loginReceive.getUsername(), loginReceive.getPassword());
        account.setOnline(true);
        return account.updateAccount(MySQL.getConnection());
    }
}
