package Server.dispatcher;

import Server.entity.app.Client;
import Server.entity.app.Lobby;
import Server.entity.app.Room;
import Server.handler.connection.ClientHandler;
import Server.server.Server;
import Server.utils.json.JsonHandler;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class BroadcastDispatcher implements IDispatcher{
    ArrayList<ClientHandler> receivers = new ArrayList<>();
    public BroadcastDispatcher() {

    }

    @Override
    public ArrayList<ClientHandler> prepareReceivers(Client client, String returnJson) {
        String role = client.getRole();
        switch (role) {
            case "user":
                switch (JsonHandler.getStringAttribute(returnJson, "command")) {
                    case "lobbynotify":
                        for(ClientHandler ch : Server.clients) {
                            if((ch.getClient().getRoom() == null) || (ch.getClient() == client))
                                receivers.add(ch);
                        }
                        break;
                    case "roomnotify":
                        JsonObject jsonObject = JsonHandler.getGson().fromJson(returnJson, JsonObject.class);
                        String roomid = jsonObject.get("room").getAsJsonObject().get("hostname").getAsString();
                        Lobby.getLobby().getRoom(roomid);
                        for(ClientHandler ch : Server.clients) {
                            try {
                                if (roomid.equals(ch.getClient().getRoom().getHostname())) receivers.add(ch);
                            } catch (Exception e) {
                                continue;
                            }
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "player":
                for(ClientHandler ch : Server.clients) {
//                    if(ch.getClient().getRoom == clientHandler.getClient().getRoom) receivers.add(ch);
                }
                break;
            default:
                break;
        }
        return receivers;
    }
}
