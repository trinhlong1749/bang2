package Server.dispatcher;

import Server.entity.app.Client;
import Server.handler.connection.ClientHandler;
import Server.server.Server;

import java.util.ArrayList;

public class ResponseDispatcher implements IDispatcher{
    ArrayList<ClientHandler> receivers = new ArrayList<>();

    @Override
    public ArrayList<ClientHandler> prepareReceivers(Client client, String returnJson) {
        for(ClientHandler ch : Server.clients)
            if(ch.getClient() == client){
                receivers.add(ch);
                break;
            }
        return receivers;
    }
}
