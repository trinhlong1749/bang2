package Server.dispatcher;

import Server.entity.app.Client;
import Server.handler.connection.ClientHandler;

import java.util.ArrayList;

public interface IDispatcher {

    public ArrayList<ClientHandler> prepareReceivers(Client client, String returnJson);
}
