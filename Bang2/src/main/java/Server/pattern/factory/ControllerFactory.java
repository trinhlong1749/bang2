package Server.pattern.factory;

import Server.controller.*;
import Server.entity.app.Client;
import Server.entity.app.Guest;

public class ControllerFactory {
    private ControllerFactory() {

    }

    public static final IController getController(String command, String role) {

        switch (command) {
            case "signup":
                if("guest".equals(role)) return new SignupController();
                else return new ErrorController();
            case "login":
                if("guest".equals(role)) return new LoginController();
                else return new ErrorController();
            case "logout":
                if("user".equals(role)) return new LogoutController();
                else return new ErrorController();
            case "createroom":
                if("user".equals(role)) return new CreateRoomController();
                else return new ErrorController();
            case "joinroom":
                if("user".equals(role)) return new JoinRoomController();
                else return new ErrorController();
            case "leaveroom":
                if("user".equals(role)) return new LeaveRoomController();
                else return new ErrorController();
            default:
                return new ErrorController();
        }
    }
}
