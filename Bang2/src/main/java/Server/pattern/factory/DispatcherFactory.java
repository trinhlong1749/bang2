package Server.pattern.factory;

import Server.dispatcher.BroadcastDispatcher;
import Server.dispatcher.IDispatcher;
import Server.dispatcher.ResponseDispatcher;

public class DispatcherFactory {

    private DispatcherFactory() {

    }

    public static IDispatcher getDispatcher(String type) {
        switch (type) {
            case "response":
                return new ResponseDispatcher();
            case "notify":
                return new BroadcastDispatcher();
            default:
                return new ResponseDispatcher();
        }
    }
}
