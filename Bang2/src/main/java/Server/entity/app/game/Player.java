package Server.entity.app.game;

import Server.entity.app.Client;

public class Player { // each thread will connect to a player
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
