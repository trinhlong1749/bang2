package Server.entity.app;

import Server.entity.app.game.Player;

import java.util.ArrayList;

public class Room {
    private ArrayList<User> members = new ArrayList<>();
    private String hostname;
    private int maxMember = 4;

    public Room(ArrayList<User> members, String hostname) {
        this.members = members;
        this.hostname = hostname;
    }

    public boolean isMax() {
        return members.size() < maxMember ? false : true;
    }
    public boolean addMember(User user) {
        members.add(user);
        return true;
    }
    public void removeMember(User user) {
        if(members.indexOf(user) == 0 && members.size() > 1) setHostname(members.get(1).getUsername());
        members.remove(user);
    }
    public Room() {
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public void setPlayers(ArrayList<User> members) {
        this.members = members;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setMembers(ArrayList<User> members) {
        this.members = members;
    }

    public int getMaxMember() {
        return maxMember;
    }

    public void setMaxMember(int maxMember) {
        this.maxMember = maxMember;
    }
}
