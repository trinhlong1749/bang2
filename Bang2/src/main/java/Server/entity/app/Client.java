package Server.entity.app;

import Server.entity.app.game.Player;

import java.io.IOException;

public class Client {
    private Guest guest;
    private User user;
    private Player player;
    private Room room;

//    private boolean broadcastLobby;
    public boolean leaveRoom() throws IOException {
        try {
            Room room = this.room;
            if (user == null) return false;
            room.removeMember(user);
            if (room.getMembers().size() == 0) {
                Lobby.getLobby().removeRoom(room);
            }
            this.setRoom(null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public Client() {
    }

    public String getUsername() {
        if (getRole().equals("guest")) return "guest";
        else if(getRole().equals("user")) return getUser().getUsername();
        else if(getRole().equals("player")) return getPlayer().getUsername();

        return null;
    }

    private void updateRole() {
        guest = null;
        user = null;
        player = null;
    }

    public void roleToGuest() {
        updateRole();
        guest = new Guest();
    }

    public void roleToUser(Guest guest, String username) {
        updateRole();
        setUser(new User(username));
    }

    public void roleToUser(Player player) {
        updateRole();
        setUser(new User(player.getUsername()));
    }

    public void roleToPlayer(User user) {
        // TODO
    }

    public String getRole() {
        if(player != null) return "player";
        else if(user != null) return "user";
        else return "guest";
    }

    public Guest getGuest() {
        return guest;
    }

    public void setGuest(Guest guest) {
        updateRole();
        this.guest = guest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        updateRole();
        this.user = user;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        updateRole();
        this.player = player;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
//    public boolean isBroadcastLobby() {
//        return broadcastLobby;
//    }
//
//    public void setBroadcastLobby(boolean broadcastLobby) {
//        this.broadcastLobby = broadcastLobby;
//    }
}
