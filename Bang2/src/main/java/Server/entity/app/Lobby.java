package Server.entity.app;

import Server.handler.connection.ClientHandler;
import Server.notifier.LobbyNotifier;

import java.io.IOException;
import java.util.ArrayList;

public class Lobby {
    private static Lobby lobby;
    private ArrayList<Room> rooms = new ArrayList<>();
    private ArrayList<Client> clients = new ArrayList<Client>();

    public boolean addClient(Client client) {
        if("user".equalsIgnoreCase(client.getRole()))
            clients.add(client);
        else clients.remove(client);
        return true;
    }

    public boolean notify(LobbyNotifier lobbyNotifier) throws IOException {
        for(Client client : clients) {
//            client.setBroadcastLobby(true);
        }
        return true;
    }

    public static Lobby getLobby() {
        if(lobby == null) {
            lobby = new Lobby();
        }
        return lobby;
    }

    private Lobby() {

    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public boolean addRoom(Room room) throws IOException {
        rooms.add(room);
        notify(new LobbyNotifier());
        return true;
    }

    public boolean removeRoom(Room room) throws IOException {
        rooms.remove(room);
        return true;
    }

    public boolean removeRoom(String roomid) throws IOException {
        for(Room room : rooms) {
            if (roomid.equalsIgnoreCase(room.getHostname())) {
                removeRoom(room);
                return true;
            }
        }
        return false;
    }

    public Room getRoom(String roomid) {
        for(Room room : rooms) if (roomid.equals(room.getHostname())) return room;
        return null;
    }
}
