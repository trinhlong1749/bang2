package Server.entity.transmission.login;

import Server.entity.app.Lobby;
import Server.entity.app.Room;
import Server.entity.transmission.Send;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginSend extends Send {
    String type = "response";
    private Map<String, Integer> lobby;
    private boolean result;

    public void setLobby(Map<String, Integer> lobby) {
        this.lobby = lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = new HashMap<>();
        ArrayList<Room> rooms = lobby.getRooms();
        for(Room r : rooms) {
            this.lobby.put(r.getHostname(), r.getMembers().size());
        }
    }

    public Map<String, Integer> getLobby() {
        return lobby;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
