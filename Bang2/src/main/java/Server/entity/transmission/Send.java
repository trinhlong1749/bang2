package Server.entity.transmission;

import Server.entity.app.Client;

import java.util.ArrayList;

public class Send {
    transient ArrayList<Client> clients = new ArrayList<Client>();
    String command;
    String msg;

    public Send() {
    }

    public Send(String command, String msg) {
        this.command = command;
        this.msg = msg;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
