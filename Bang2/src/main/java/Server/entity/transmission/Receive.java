package Server.entity.transmission;

import Server.utils.resources.Path;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;

public class Receive {

    private String command;

    public Receive() {

    }

    public Receive(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
//    public static ArrayList<String> getReceiveCommands() {
//        try (InputStream input = new FileInputStream(Path.getPathFromResources("receive.properties"))) {
//            ArrayList<String> commands = new ArrayList<>();
//            Properties prop = new Properties();
//
//            // load a properties file
//            prop.load(input);
//
//            prop.forEach((k, v) -> commands.add(v.toString()));
////            prop.forEach((k, v) -> System.out.println(v.toString()));
//
//            return commands;
//        } catch (IOException | URISyntaxException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }
//
//    public static void main(String[] args) {
//        try (InputStream input = new FileInputStream(Path.getPathFromResources("receive.properties"))) {
//
//            Properties prop = new Properties();
//
//            // load a properties file
//            prop.load(input);
//
////            for (String s : prop.stringPropertyNames()) System.out.println(s);
//            getReceiveCommands();
//
//
//        } catch (IOException | URISyntaxException ex) {
//            ex.printStackTrace();
//        }
//    }
}
