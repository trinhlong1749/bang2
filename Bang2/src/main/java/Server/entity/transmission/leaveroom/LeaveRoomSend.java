package Server.entity.transmission.leaveroom;

import Server.entity.transmission.Send;

public class LeaveRoomSend extends Send {
    boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
