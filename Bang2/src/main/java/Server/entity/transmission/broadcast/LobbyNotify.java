package Server.entity.transmission.broadcast;

import Server.entity.app.Room;
import Server.entity.transmission.Send;

import java.util.ArrayList;

public class LobbyNotify extends Send {
    String type = "notify";
    ArrayList<Room> rooms = new ArrayList<Room>();

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
