package Server.entity.transmission.broadcast;

import Server.entity.app.Room;
import Server.entity.app.User;
import Server.entity.app.game.Player;
import Server.entity.transmission.Send;

import java.util.ArrayList;

public class RoomNotify extends Send {
    String type = "notify";
    Room room;
    ArrayList<User> members;

    public RoomNotify(String command, String msg, String type, Room room, ArrayList<User> members) {
        super(command, msg);
        this.type = type;
        this.room = room;
        this.members = members;
    }

    public RoomNotify() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<User> members) {
        this.members = members;
    }
}
