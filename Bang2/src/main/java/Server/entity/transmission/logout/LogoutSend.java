package Server.entity.transmission.logout;

import Server.entity.transmission.Send;

public class LogoutSend extends Send {
    String type = "response";
    boolean result;

    public LogoutSend() {

    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
