package Server.entity.transmission.logout;

import Server.entity.transmission.Receive;

public class LogoutReceive extends Receive {

    String username;

    public LogoutReceive(String command) {
        super(command);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
