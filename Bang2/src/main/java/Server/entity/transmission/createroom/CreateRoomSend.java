package Server.entity.transmission.createroom;

import Server.entity.transmission.Send;

public class CreateRoomSend extends Send {
    String type = "response";
    private boolean result;
    private String roomid;

    public CreateRoomSend(String command, String msg, boolean result, String roomid) {
        super(command, msg);
        this.result = result;
        this.roomid = roomid;
    }

    public CreateRoomSend() {
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
