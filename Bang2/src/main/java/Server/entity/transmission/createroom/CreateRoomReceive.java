package Server.entity.transmission.createroom;

import Server.entity.transmission.Receive;

public class CreateRoomReceive extends Receive {
    public CreateRoomReceive() {
    }

    public CreateRoomReceive(String command) {
        super(command);
    }
}
