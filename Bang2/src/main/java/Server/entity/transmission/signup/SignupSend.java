package Server.entity.transmission.signup;

import Server.entity.transmission.Send;
import Server.utils.json.JsonHandler;

public class SignupSend extends Send {
    String type = "response";
    boolean result;

    public SignupSend() {
        super();
    }

    public SignupSend(String command, String msg, boolean result) {
        super(command, msg);
        this.result = result;
    }

    public SignupSend(String returnJson) {
        setMsg(JsonHandler.getGson().fromJson(returnJson, getClass()).getMsg());
        setCommand(JsonHandler.getGson().fromJson(returnJson, getClass()).getCommand());
        result = (JsonHandler.getGson().fromJson(returnJson, getClass()).isResult());
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
