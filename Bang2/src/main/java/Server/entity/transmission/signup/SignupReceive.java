package Server.entity.transmission.signup;

import Server.entity.transmission.Receive;

public class SignupReceive extends Receive {
    private String username;
    private String password;

    public SignupReceive() {
    }

    public SignupReceive(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
