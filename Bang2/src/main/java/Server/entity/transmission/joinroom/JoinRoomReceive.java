package Server.entity.transmission.joinroom;

import Server.entity.transmission.Receive;

public class JoinRoomReceive extends Receive {
    String username;
    String roomid;

    public JoinRoomReceive(String command, String username, String roomid) {
        super(command);
        this.username = username;
        this.roomid = roomid;
    }

    public JoinRoomReceive() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }
}
