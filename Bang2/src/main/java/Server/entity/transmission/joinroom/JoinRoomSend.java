package Server.entity.transmission.joinroom;

import Server.entity.app.User;
import Server.entity.transmission.Send;

import java.util.ArrayList;

public class JoinRoomSend extends Send {
    String type = "response";
    boolean result;
    ArrayList<User> roomMembers = new ArrayList<User>();

    public JoinRoomSend() {
    }

    public JoinRoomSend(String command, String msg, boolean result, ArrayList<User> roomMembers) {
        super(command, msg);
        this.result = result;
        this.roomMembers = roomMembers;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ArrayList<User> getRoomMembers() {
        return roomMembers;
    }

    public void setRoomMembers(ArrayList<User> roomMembers) {
        this.roomMembers = roomMembers;
    }
}
