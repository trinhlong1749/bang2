package Server.notifier;

import Server.entity.app.Client;
import Server.entity.app.Lobby;
import Server.entity.transmission.broadcast.LobbyNotify;
import Server.utils.json.JsonHandler;

public class LobbyNotifier {
    LobbyNotify lobbyNotify = new LobbyNotify();

    public String prepareNotify() {
//        if (!client.isBroadcastLobby()) return null;
        lobbyNotify.setCommand("lobbynotify");
        lobbyNotify.setMsg("Notifying lobby status");
        lobbyNotify.setRooms(Lobby.getLobby().getRooms());
        String returnJson = JsonHandler.getGson().toJson(lobbyNotify);
//        client.setBroadcastLobby(false);
        return returnJson;
    }
}
