#create database netprog;

use netprog;

create table `account` (
	username varchar(50) primary key,
    `password` varchar(50),
    `online` boolean)